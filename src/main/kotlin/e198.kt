@file:MavenRepository("jitpack", "https://jitpack.io")
@file:DependsOn("com.github.hj-lee:kt-rational:v0.2")

import io.github.hj_lee.rational.longrational.gcd
import kotlin.system.measureTimeMillis

fun meanDenominator(a: Long, b: Long) = a * b * 2

private fun getElapsedTime(start: Long) = (System.nanoTime() - start).toDouble() / 1000000000.0

data class NumerInc(val numerator: Long, val inc: Long) : Comparable<NumerInc> {
    fun getDenominator(base: Long) = base * numerator + inc

    fun getMirror() = NumerInc(numerator, numerator - inc)

    override fun compareTo(other: NumerInc): Int = (numerator * other.inc - other.numerator * inc).toInt()

    override fun toString(): String = "($numerator/base+$inc)"
}

data class AmbiguousPair(val prev: NumerInc, val next: NumerInc) : Comparable<AmbiguousPair> {
    fun getDenominator(base: Long) = meanDenominator(prev.getDenominator(base), next.getDenominator(base))

    override fun compareTo(other: AmbiguousPair): Int = this.getDenominator(1).compareTo(other.getDenominator(1))

    override fun toString(): String = "{$prev:$next}"
}

fun findBaseAmbiguousPairs(base: Long, maxDenominator: Long): MutableList<AmbiguousPair> {
    if (meanDenominator(base, base + 1) > maxDenominator) return mutableListOf()
    val set = sortedSetOf(NumerInc(1, 0))
    val pairs = mutableListOf(AmbiguousPair(NumerInc(1, 0), NumerInc(1, 1)))
    val maxNumerator = maxDenominator / (2 * base * base)

    for (n in 2..maxNumerator) {
        for (inc in 1..(n / 2)) {
            if (gcd(n, inc) != 1L) continue
            var found = false
            val numInc = NumerInc(n, inc)
            val denom = numInc.getDenominator(base)
            val mirror = numInc.getMirror()
            val mirrorDenom = mirror.getDenominator(base)
            val next = set.higher(numInc)
            val nMirror = next.getMirror()
            if (meanDenominator(nMirror.getDenominator(base), mirrorDenom) <= maxDenominator) {
                found = true
                pairs += AmbiguousPair(nMirror, mirror)
                pairs += AmbiguousPair(numInc, next)
            } else if (meanDenominator(next.getDenominator(base), denom) <= maxDenominator) {
                found = true
                pairs += AmbiguousPair(numInc, next)
            }
            val prev = set.lower(numInc)
            if (prev != null) {
                val pMirror = prev.getMirror()
                if (meanDenominator(pMirror.getDenominator(base), mirrorDenom) <= maxDenominator) {
                    found = true
                    pairs += AmbiguousPair(mirror, pMirror)
                    pairs += AmbiguousPair(prev, numInc)
                } else if (meanDenominator(prev.getDenominator(base), denom) <= maxDenominator) {
                    found = true
                    pairs += AmbiguousPair(prev, numInc)
                }
            }
            if (found) set += numInc
        }
//        if (n % 1000 == 0L){
//            println("$n")
//        }
    }
    return pairs
}


fun countAmbiguousPairs(base: Long, maxDenominator: Long): Long {
    val start = System.nanoTime()
    val pairs = findBaseAmbiguousPairs(base, maxDenominator)
    println("find pairs ${getElapsedTime(start)}")
    return pairs.size + generateSequence(base + 1) { it + 1 }.map { currentBase ->
        pairs.removeIf {
            it.getDenominator(currentBase) > maxDenominator
        }
        val cnt = pairs.size
//        if (currentBase % 50 == 0L) println("find pairs $currentBase - $cnt - ${getElapsedTime(start)} sec")

        cnt
    }.takeWhile { it > 0 }.sum() + maxDenominator / 2 - base / 2
}

fun main() {
    val time = measureTimeMillis {
//        for (i in (1..5)) {
//            println(countAmbiguousPairs(100, 100000000))
//        }
        println(countAmbiguousPairs(100, 100000000))
    }
    // 52374425
    println("${time.toDouble() / 1000.0} sec")
}
