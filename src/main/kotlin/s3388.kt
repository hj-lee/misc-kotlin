import com.google.common.collect.HashMultiset
import com.google.common.collect.Multiset
import io.github.hj_lee.rational.longrational.Rational
import io.github.hj_lee.rational.longrational.r

fun <E> multisetOf(vararg elms: E) = HashMultiset.create(elms.asIterable())

fun <E> List<E>.combinations(n: Int): Set<Multiset<E>> {
    if (this.isEmpty() || n <= 0) return emptySet()
    return when (n) {
        1 -> this.map { multisetOf(it) }.toSet()
        else -> {
            this.distinct().flatMap { elm ->
                (this - elm).combinations(n - 1).map {
                    it.add(elm)
                    it
                }
            }.toSet()
        }
    }
}

fun <E> Multiset<E>.combinations(n: Int): Set<Multiset<E>> = this.toList().combinations(n)

fun fourOps(xpair: Pair<Rational, String>, ypair: Pair<Rational, String>): Map<Rational, String> {
    val (x, xstr) = xpair
    val (y, ystr) = ypair
    var dict = mapOf(
        x + y to "($xstr + $ystr)",
        x - y to "($xstr - $ystr)",
        y - x to "($ystr - $xstr)",
        x * y to "($xstr * $ystr)"
    )
    if (y != 0.r) dict += x / y to "($xstr / $ystr)"
    if (x != 0.r) dict += y / x to "($ystr / $xstr)"
    return dict
}

fun merge(m1: Map<Rational, String>, m2: Map<Rational, String>) = m1 + m2

fun fourOpsMap(d1: Map<Rational, String>, d2: Map<Rational, String>) =
    d1.map { kv ->
        d2.map { kv2 ->
            fourOps(kv.toPair(), kv2.toPair())
        }.reduce(::merge)
    }.reduce(::merge)

fun buildResultOperationMap(mapSet: Multiset<Map<Rational, String>>): Map<Rational, String> {
    require(mapSet.isNotEmpty())
    return when (mapSet.size) {
        1 -> mapSet.first()
        2 -> mapSet.reduce(::fourOpsMap)
        else -> {
            val combinations = mapSet.combinations(2)
//            println(combinations)
            combinations.map { comb ->
                val (x, y) = comb.toList()
                val newMapSet = HashMultiset.create(mapSet)
                comb.forEach { newMapSet -= it }
                newMapSet += fourOpsMap(x, y)
                buildResultOperationMap(newMapSet)
            }.reduce(::merge)
        }
    }
}



fun buildResultOperationMap(): Map<Rational, String> {
    fun numToMap(x: Number) = mapOf(x.r to "$x")
    val s3 = numToMap(3)
    val s8 = numToMap(8)
    return buildResultOperationMap(multisetOf(s3, s3, s8, s8))
}

fun main() {
    val dict = buildResultOperationMap()
//    println(dict.size)
//    dict.toList().sortedBy { it.first }.forEach { println("${it.first} =\t ${it.second} ") }
    println("${dict[24.r]} = 24")
}